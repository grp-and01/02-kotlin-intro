fun main(args: Array<String>) {
    bonus3(args)
}
fun u1() {
    println("Hello world")
}
fun u2() {
    for (i in 1..10) {
        println("Hello world")
    }
}
fun u3(args: Array<String>) {
    println(args[0])
}
fun u4(args: Array<String>) {
    val num = args[0].toInt()
    if (num % 2 == 0) {
        println("Even")
    } else {
        println("Odd")
    }
}
fun u5(args: Array<String>) {
    val num = args[0].toInt()
    for (i in 1..num) {
        print("*")
    }
}
fun u6(args: Array<String>) {
    val width = args[0].toInt()
    val height = args[1].toInt()
    for (i in 1..height) {
        for (j in 1..width) {
            print("*")
        }
        println()
    }
}
fun u7(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in 1..rows) {
        for (j in 1..i) {
            print("*")
        }
        println()
    }
}
fun u8(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in 1..rows) {
        for (j in i..rows) {
            print(" ")
        }
        for (j in 1..i) {
            print("*")
        }
        println()
    }
}
fun u9(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in 1..rows) {
        for (j in i..rows) {
            print(" ")
        }
        for (j in 1..2*i-1) {
            print("*")
        }
        println()
    }
}
fun u10(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in 1..rows) {
        for (j in i..rows) {
            print(" ")
        }
        for (j in 1..2*i-1) {
            if (j % 2 == 0) {
                print("#")
            } else {
                print("*")
            }
        }
        println()
    }
}
fun u11(args: Array<String>) {
    val rows = args[0].toInt()
    var currentNumber = 1
    for (i in 1..rows) {
        for (j in 1..i) {
            print(currentNumber)
            print(" ")
            currentNumber = currentNumber + 1
        }
        println()
    }
}
fun u12(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in 1..rows) {
        for (j in i..rows) {
            print(" ")
        }
        for (j in 1..i) {
            if (j <= i/2) {
                print("$j ")
            } else {
                print("${i-j+1} ")
            }
        }
        println()
    }
}
fun bonus1(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in 0..rows-1) {
        for (j in i..rows) {
            print(" ")
        }
        for (j in 0..i) {
            val value = factorial(i) / ( factorial(i-j) * factorial(j))
            System.out.printf("%2d ", value)
        }
        println()
    }
}
fun factorial(i: Int): Int {
    var f = 1
    for (x in 1..i) {
        f *=x
    }
    return f
}
fun bonus2(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in rows downTo 1) {
        for (space in 1..rows - i) print("  ")
        for (j in 1..2 * i - 1 step 2) print("$j ")
        println()
    }
}
fun bonus3(args: Array<String>) {
    val rows = args[0].toInt()
    for (i in rows downTo 0) {
        for (space in 1..rows - i) print("  ")
        for (j in i..2 * i - 1) print("$j ")
        for (j in 2 * i - 2 downTo i) print("$j ")
        println()
    }
}

